import io from 'socket.io-client';
import $ from 'jquery';

const socket = io('//localhost:3000');
var initial = true; //just for demo purpose, to handle refresh screen will start from beginning.

$('form').submit(function(){
  var msg = $('#chat-input').val();
  socket.emit('chat message', JSON.stringify({msg: msg, ts: (new Date()).getTime(), initial: initial}));
  $('#messages').append($('<li class="me">').text($('#chat-input').val()));
  $('#chat-input').val('');
  initial = false;
  return false;
});

socket.on('chat message', function(msg){
  $('#messages').append($('<li>').text(msg));
});
