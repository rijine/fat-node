## Chat Bot

### Conversation Samples

**user**: hello<br/>
**bot**: Hello, Welcome to the Adlon Kempinski online booking bot!, Let me know how can I help you?

**user**: I'm looking for a room<br/>
**bot**: Great, We provide best price and quality service in Berlin. We have Xmas offers running. All rooms are in 20% off price!<br/>

**user**: do you have any other offers?<br/>
**bot**: Other than 20% off on booking, we provide complimentary breakfast too.<br/>

**user**: what is nearby attractions?<br/>
****We are middle of the city, We have casinos, shopping malls and parks all are near by.<br/>

**user**:what kind of rooms are available?<br/>
**bot**: We have Deluxe Room, Superior Deluxe Room and Executive Rooms.<br/>

**user**:price range for each?<br/>
**bot**: Deluxe rooms from $200/day, Superior Deluxe rooms from $450/day and Executive $1000/day.<br/>

**user**:Can you check availability of Deluxe room?<br/>
**bot**: Sure<br/>

**bot**: May I know which dates do you want to reserve for? (eg. Jan 22 to 25)<br/>
**user**:Jan 25 to 30<br/>

**bot**: May know how many rooms do you want reserve?<br/>
**user**:1<br/>
**bot**: Rooms are available!<br/>

**bot**: Shall I proceed with booking?<br/>
**user**:yes<br/>

**bot**: May I know your name?<br/>
**user**:Alice<br/>

**bot**: Let me know your contact no?<br/>
**user**:9876543210<br/>

**bot**: Name: Alice, Contact: 9876543210, Room: deluxe, Date: Jan 25 to 30 , No of Rooms: 1, Total : $2000.00<br/>
**bot**: Is the informations provided are valid?<br/>
**user**:yes<br/>

**bot**:Please confirm your booking by entering Confirmation Code received in you mobile!<br/>
**user**:667 999<br/>

**bot**: Your booking is confirmed. You will receive sms soon.<br/>
**bot**: Is there anything else I can help you with?<br/>
**user**:no<br/>

**bot**: Thank you, Have a nice day & Happy Xmas & Happy New year<br/>
**user**: bye<br/>
**bot**: bye
