var assert = require('assert');
var sinon = require('sinon');
var CostService = require('./../../src/services/cost.service');

describe('CostService', function() {
    it('#setProperties & #getProperties', function () {
        CostService.setProperties('Alice', 'name');
        assert.equal(CostService.getProperties('name'), 'Alice', 'Name is undefined');
    });

    it('#calculate', function () {
        assert.equal(CostService.calculate(), '$2000.00', 'Amount mis match');
    });

    it('#setBookingQuestion & #getBookingQuestion', function () {
        CostService.setBookingQuestion(1);
        assert.equal(CostService.getBookingQuestion(), 1, 'Qn no mis match');
    });

});