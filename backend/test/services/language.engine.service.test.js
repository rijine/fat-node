var assert = require('assert');
var sinon = require('sinon');
const LanguageEngine = new (require('./../../src/services/language.engine.service'))();
var CostService = require('./../../src/services/cost.service');

describe('LanguageService', function() {
    it('#findReply', function () {
        let msg = { msg: 'hi', ts: 12312, initial: false};
        LanguageEngine.findReply(msg);
        msg = { msg: 'hi', ts: 12312, initial: true};
        LanguageEngine.findReply(msg);

        msg = { msg: 'hi', ts: 12312, initial: false};
        CostService.setProperties('superior', 'roomType');
        CostService.setBookingQuestion(2);
        LanguageEngine.findReply(msg);

        //CostService.setProperties('superior', 'roomType');
        CostService.setBookingQuestion(6);
        LanguageEngine.findReply(msg);

        //cover next levels
        LanguageEngine.findReply(msg);

    });

    it('#bookingQuestions', function () {
        let msg = { msg: 'ok', ts: 12312, initial: false};

        CostService.setBookingQuestion(1);
        LanguageEngine.bookingQuestions(msg);

        LanguageEngine.bookingQuestions(msg);

        msg = { msg: 'Alice', ts: 12312, initial: false};
        LanguageEngine.bookingQuestions(msg);

        msg = { msg: 'Jan 25 to 30', ts: 12312, initial: false};
        LanguageEngine.bookingQuestions(msg);

        msg = { msg: 'Yes', ts: 12312, initial: false};
        LanguageEngine.bookingQuestions(msg);

        LanguageEngine.bookingQuestions(msg);
        LanguageEngine.bookingQuestions(msg);

        LanguageEngine.bookingQuestions(msg); //for level q8
    });


    describe('#processReply', function () {

        it('#processReply -offer', function () {
            let msg = { msg: 'offer', ts: 12312, initial: false};
            let reply = LanguageEngine.processReply(msg);

            msg.msg = 'without';
            reply = LanguageEngine.processReply(msg);
        });

        it('#processReply -price', function () {
            let msg = { msg: 'price', ts: 12312, initial: false};
            let reply = LanguageEngine.processReply(msg);
        });

        it('#processReply -bye', function () {
            let msg = { msg: 'bye', ts: 12312, initial: false};
            let reply = LanguageEngine.processReply(msg);
        });

        it('#processReply -Attraction', function () {
            let msg = { msg: 'attraction', ts: 12312, initial: false};
            let reply = LanguageEngine.processReply(msg);

            msg.msg = 'looking';
            reply = LanguageEngine.processReply(msg);

        });

        it('#processReply -Room', function () {
            let msg = { msg: 'kind', ts: 12312, initial: false};
            let reply = LanguageEngine.processReply(msg);

            msg.msg = 'Deluxe';
            reply = LanguageEngine.processReply(msg);

            msg.msg = 'Superior';
            reply = LanguageEngine.processReply(msg);

            msg.msg = 'Executive';
            reply = LanguageEngine.processReply(msg);
        });

    });

});