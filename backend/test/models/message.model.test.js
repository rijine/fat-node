var assert = require('assert');
var sinon = require('sinon');
var Message = require('./../../src/models/message.model');

describe('MessageModel', function() {
    it('#constructor', function () {
        let msg = new Message('hello');
        assert.equal(msg.getMsg(), 'hello', 'Message init failed');

        let msg2 = new Message('hi', 3023023);
        assert.equal(msg2.timestamp, 3023023, 'Message init failed with timestamp');
    });

});