/**
 * class to store messages ( for now storing message and ts)
 * (skips: author and other infos)
 */
class Message {
  /**
    * accepts message and user interaction timestamp and assign to object props
    * @param msg
    * @param timestamp
    */
  constructor(msg, timestamp) {
    this.msg = msg;
    if(timestamp){
      this.timestamp = timestamp;
    } else {
      this.timestamp = (new Date()).getTime(); //Recommended moment.now();
    }
  }

  /**
    *
    * @returns returns msg text;
    */
  getMsg(){
    return this.msg;
  }
}

module.exports = Message;
