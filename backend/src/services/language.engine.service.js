const BotMessages = require('../data/bot.messages');
const Message = require('../models/message.model');
const CostService = require('../services/cost.service');

/**
 * Class responsible for returning appropriate user messages
 */
class LanguageEngine {

  /**
   * Receives natural messages from user and appropriate reply from NLP engine(mock now)
   * @param incomingMsg
   * @returns {Array}
   */
  findReply(incomingMsg) {
    let res = [];
    const lastQn = 9;
    const firstQn = 1;

    if (incomingMsg.initial) {

      CostService.setBookingQuestion(firstQn);
      res = [new Message(BotMessages.common.welcome)]; //initial reply
    } else if (CostService.room.roomType
        && (CostService.getBookingQuestion() <= lastQn && CostService.getBookingQuestion() > firstQn )) {
      res = this.bookingQuestions(incomingMsg);
    } else {
      res = this.processReply(incomingMsg);
    }

    return res;
  }

  /**
     * If User decided to proceed with booking, this fn will handles to capture user information
     * @param incomingMsg
     * @returns {Array}
     */
  bookingQuestions(incomingMsg) {
    let qn = CostService.getBookingQuestion();
    let reply = [];
    let nextQn = qn + 1;
    const confrim = 6;
    if(qn === confrim){
      console.log(incomingMsg.msg, BotMessages.bookingQuestions['q' + (qn - 1)].key);
      CostService.setProperties(incomingMsg.msg, BotMessages.bookingQuestions['q' + (qn - 1)].key);
      CostService.setBookingQuestion(nextQn);
      reply = [
        new Message( 'Name: ' + CostService.getProperties('name') +
          ', Contact: ' + CostService.getProperties('contact') +
          ', Room: ' + CostService.getProperties('roomType') +
          ', Date: ' + CostService.getProperties('dates') +
          ', No of Rooms: ' + CostService.getProperties('rooms') +
          ', Total : ' + CostService.calculate() ),
        new Message(BotMessages.bookingQuestions['q' + (qn)].qn),
      ];
    } else if (qn > 1) {
      if (BotMessages.bookingQuestions['q' + (qn-1)].reply) {
        console.log(incomingMsg.msg, BotMessages.bookingQuestions['q' + (qn - 1)].key);
        reply = [new Message(BotMessages.bookingQuestions['q' + (qn-1)].reply)];
      }
      CostService.setProperties(incomingMsg.msg, BotMessages.bookingQuestions['q' + (qn - 1)].key);
      if(BotMessages.bookingQuestions['q' + (qn)].qn){
        reply.push(new Message(BotMessages.bookingQuestions['q' + (qn)].qn));
      }
      CostService.setBookingQuestion(nextQn);

    } else {
      CostService.setBookingQuestion(nextQn);
      reply = [new Message(BotMessages.bookingQuestions['q1'].qn)];
    }
    return reply;
  }

  /**
   *
   * @param incoming
   * @returns array of replies
   */
  processReply(incoming) {
    let reply = [];
    console.log(incoming);
    if (incoming.msg.indexOf(BotMessages.offerAndBooking.index) > -1) {
      reply = [new Message(BotMessages.offerAndBooking.reply)];
    } else if (incoming.msg.indexOf(BotMessages.attractions.index) > -1) {
      reply = [new Message(BotMessages.attractions.reply)];
    } else if (incoming.msg.indexOf(BotMessages.rooms.index) > -1) {
      reply = [new Message(BotMessages.rooms.reply)];
    } else if (incoming.msg.indexOf(BotMessages.offers.index) > -1) {
      reply = [new Message(BotMessages.offers.reply)];
    } else if (incoming.msg.indexOf(BotMessages.price.index) > -1) {
      reply = [new Message(BotMessages.price.reply)];
    } else if (incoming.msg.indexOf(BotMessages.isWithOffer.index) > -1) {
      reply = [new Message(BotMessages.isWithOffer.reply)];
    } else if (incoming.msg.indexOf(BotMessages.proceed.superior.index) > -1) {
      CostService.setProperties('superior', 'roomType');
      reply = [new Message(BotMessages.proceed.superior.reply)];
    } else if (incoming.msg.indexOf(BotMessages.proceed.deluxe.index) > -1) {
      CostService.setProperties('deluxe', 'roomType');
      reply = this.bookingQuestions(incoming);
      reply = [new Message(BotMessages.proceed.deluxe.reply), ...reply];
    } else if (incoming.msg.indexOf(BotMessages.proceed.executive.index) > -1) {
      CostService.setProperties('executive', 'roomType');
      reply = [new Message(BotMessages.proceed.executive.reply)];
    } else if(incoming.msg.indexOf(BotMessages.bye.index) > -1) {
      reply = [new Message(BotMessages.bye.ans)];
    }
    return reply;
  }
}

module.exports = LanguageEngine;