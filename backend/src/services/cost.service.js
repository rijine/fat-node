// class CostService {}

const CostService = {
  room: {
    question: 1
  },
  setProperties: (prop, key) => {
    CostService.room[key] = prop;
  },
  getProperties: (key) => {
    return CostService.room[key];
  },
  calculate: () => {
    return '$2000.00';
  },
  setBookingQuestion(qn){
    CostService.room.question = qn;
  },
  getBookingQuestion(){
    return CostService.room.question;
  }

};

module.exports = CostService;