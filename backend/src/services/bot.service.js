const LanguageEngine = new (require('../services/language.engine.service'))();

/**
 *  Class responsible for interacting with bot and human
 */
class BotService {
  /**
     * Bot Service is calling Language service to return proper reply
     * @param incomingMsg what ever user sends here accepts
     */
  getReply(incomingMsg){
    return LanguageEngine.findReply(incomingMsg);
  }
}


module.exports = BotService;