const BotMessages = {
  common: {
    welcome: 'Hello, Welcome to the Adlon Kempinski online booking bot!, Let me know how can I help you?',
    whatIsThis: 'Sorry, I didn\'t get that.'
  },
  offerAndBooking: { //2
    msg: 'I \'Looking for a room?',
    index: 'looking',
    reply: 'Great, We provide best price and quality service in Berlin.' +
    ' We have Xmas offers running.' +
    ' All rooms are in 20% off price!'
  },
  offers: {
    msg: 'Do you have any other offers?', //3
    index: 'offer',
    reply: 'Other than 20% off on booking, we provide complimentary breakfast too.'
  },
  attractions: {
    msg: 'What are the near by attractions?',
    index: 'attraction',
    reply: 'We are middle of the city, We have casinos, shopping malls and parks all are near by.'
  },
  rooms: {
    msg: 'What kind of rooms are available?',
    index: 'kind',
    reply: 'We have Deluxe Room, Superior Deluxe Room and Executive Rooms.'
  },
  price: {
    msg: 'Can tell me about price ?',
    index: 'price',
    reply: 'Deluxe rooms from $200/day, Superior Deluxe rooms from $450/day and Executive $1000/day.'
  },
  isWithOffer: {
    msg: 'This is price is with/without offer?',
    index: 'with',
    reply: 'These prices are without offer'
  },
  proceed: {
    deluxe: {
      msg: 'Can you check availability of Deluxe Room?',
      index: 'Deluxe',
      reply: 'Sure'
    },
    superior: {
      msg: 'Can you check availability of Superior Deluxe Room?',
      index: 'Superior',
      reply: 'Sure'
    },
    executive: {
      msg: 'Can you check availability of Executive Room?',
      index: 'Executive',
      reply: 'Sure'
    }
  },
  bookingQuestions: {
    q1: {
      qn: 'May I know which dates do you want to reserve for? (eg. Jan 22 to 25)',
      key: 'dates'
    },
    q2: {
      qn: 'May know how many rooms do you want reserve?',
      key: 'rooms',
      reply: 'Rooms are available!'
    },
    q3: {
      qn:  'Shall I proceed with booking?',
      key: 'accepts'
    },
    q4: {
      qn: 'May I know your name?',
      key: 'name'
    },
    q5: {
      qn: 'Let me know your contact no?',
      key: 'contact'
    },
    q6: {
      qn: 'Is the informations provided are valid?',
      key: 'confirm'
    },
    q7: {
      qn: 'Please confirm your booking by entering Confirmation Code received in you mobile!',
      key: 'name',
      reply: 'Your booking is confirmed. You will receive sms soon.'
    },
    q8: {
      qn: 'Is there anything else I can help you with?',
      key: 'more',
      reply: 'Thank you, Have a nice day & Happy Xmas & Happy New year'
    },
    q9: {
    }
  },
  bye: {
    index: 'bye',
    ans: 'bye'
  },
  hi: {
    index: ['hi', 'hello'],
    reply: 'hi'
  },
  thanks: {
    index: ['thanks', 'thank you', 'regards'],
    reply: 'You are welcome'
  }
};

module.exports = BotMessages;